#include <iostream>
#include <vector>
#include <algorithm>
#include "rectangle.h"

double bssf_score(const Rectangle&, const Rectangle&);
std::vector<Rectangle> sas_rule(const Rectangle&, const Rectangle&);

int main(){
    double big_number = 1e300;
    Rectangle table(Point(100.0, 80.0));
    Rectangle tree_stand(Point(30.0, 30.0));
    bool standPlaced = false;

    std::vector<Rectangle> gifts = {
            Rectangle(Point(20.0, 10.0)),
            Rectangle(Point(10.0, 11.0)),
            Rectangle(Point(3.0, 46.0)),
            Rectangle(Point(3.0, 4.0)),
            Rectangle(Point(6.0, 16.0)),
            Rectangle(Point(10.0, 20.0)),
            Rectangle(Point(20.0, 8.0)),
            Rectangle(Point(12.0, 37.0)),
            Rectangle(Point(11.0, 15.0)),
            Rectangle(Point(40.0, 63.0)),
            Rectangle(Point(23.0, 6.0)),
            Rectangle(Point(16.0, 12.0)),
            Rectangle(Point(25.0, 20.0)),
            Rectangle(Point(67.0, 3.0)),
            Rectangle(Point(31.0, 29.0)),
            Rectangle(Point(12.0, 11.0)),
            Rectangle(Point(8.0, 9.0)),
            Rectangle(Point(3.0, 8.0)),
            Rectangle(Point(21.0, 13.0)),
            Rectangle(Point(46.0, 13.0)),
            Rectangle(Point(11.0, 75.0)),
            Rectangle(Point(4.0, 3.0)),
            Rectangle(Point(19.0, 7.0)),
            Rectangle(Point(33.0, 7.0)),
            Rectangle(Point(6.0, 16.0)),
            Rectangle(Point(21.0, 4.0)),
            Rectangle(Point(8.0, 8.0)),
            Rectangle(Point(3.0, 86.0)),
            Rectangle(Point(20.0, 6.0)),
            Rectangle(Point(21.0, 3.0)),
            Rectangle(Point(13.0, 59.0)),
            Rectangle(Point(4.0, 20.0))
    };
    std::vector<Rectangle> to_be_placed = gifts;
    std::vector<Rectangle> already_placed;
    std::vector<Rectangle> free_rectangles = {table};

    bool impossible;

    do{
        impossible = true;
        bool transposed = false;
        int minI = 0;
        int minII = 0;


        for (int i = 0; i < free_rectangles.size(); i++){
            for (int ii = 1; ii < to_be_placed.size(); ii++){
                if (bssf_score(free_rectangles[i], to_be_placed[ii]) <= bssf_score(free_rectangles[minI], to_be_placed[minII])){
                    minI = i;
                    minII = ii;
                    impossible = false;
                }
            }
        }
        for (int i = 0; i < free_rectangles.size(); i++){
            for (int ii = 1; ii < to_be_placed.size(); ii++){
                if (bssf_score(free_rectangles[i], to_be_placed[ii].transpose()) <= bssf_score(free_rectangles[minI], to_be_placed[minII])){
                    transposed = true;
                    minI = i;
                    minII = ii;
                    impossible = false;
                }
            }
        }

        if (!impossible && bssf_score(free_rectangles[minI], to_be_placed[minII]) != big_number){
            Rectangle best_free = free_rectangles[minI];
            Rectangle best_obj = to_be_placed[minII];

            free_rectangles.erase(free_rectangles.begin() + minI);
            std::vector<Rectangle> pushVector = sas_rule(best_free, best_obj);

            bool tree_stand_fits = false;

            if (!standPlaced){
                //prüfen ob ständer in eins der freien rechtecke reinpasst
                for (Rectangle r : free_rectangles) if (bssf_score(r, tree_stand) != big_number) tree_stand_fits = true;
                for (Rectangle r : pushVector) if (bssf_score(r, tree_stand) != big_number) tree_stand_fits = true;
            }
            else tree_stand_fits = true;

            if (tree_stand_fits){
                if (transposed) best_obj.transpose();

                best_obj.translate(Point(best_free.getX0() - best_obj.getX0(), best_free.getY0() - best_obj.getY0()));

                already_placed.push_back(best_obj);
                to_be_placed.erase(to_be_placed.begin() + minII);

                for (Rectangle r : pushVector) free_rectangles.push_back(r);
            }
            else{
                free_rectangles.push_back(best_free);

                bool transposedS = false;
                int minIS = 0;
                for (int i = 1; i < free_rectangles.size(); i++){
                    if (bssf_score(free_rectangles[i], tree_stand) < bssf_score(free_rectangles[minIS], tree_stand)) minIS = i;
                }
                for (int i = 1; i < free_rectangles.size(); i++){
                    if (bssf_score(free_rectangles[i], tree_stand.transpose()) < bssf_score(free_rectangles[minIS], tree_stand.transpose())){
                        minIS = i;
                        transposedS = true;
                    }
                }
                tree_stand.translate(Point(tree_stand.getX0() - tree_stand.getX0(), tree_stand.getY0() - tree_stand.getY0()));
                already_placed.push_back(tree_stand);
                free_rectangles.erase(free_rectangles.begin() + minIS);

                pushVector = sas_rule(free_rectangles[minIS], tree_stand);
                for (Rectangle r : pushVector) free_rectangles.push_back(r);

                standPlaced = true;
            }
        }
    }while(!impossible);

    if (!standPlaced) std::cout << "Huch..\nDer Weihnachtsbaum hat keinen Platz mehr..\n";

    return 0;
}

double bssf_score(const Rectangle& free, const Rectangle& obj){
    double big_number = 1e300;
    double remainingWidth = free.width() - obj.width();
    double remainingHeight = free.height() - obj.height();

    if (remainingWidth < 0 || remainingHeight < 0) return big_number;

    return min(remainingWidth, remainingHeight);
}
std::vector<Rectangle> sas_rule(const Rectangle& free, const Rectangle& best){
    std::vector<Rectangle> output;
    if (free.width() < free.height()){
        output.push_back(Rectangle(Point(best.getX1(), free.getY0()), Point(free.getX1(), best.getY1())));
        output.push_back(Rectangle(Point(free.getX0(), best.getY1()), free.getP1()));
    }
    else{
        output.push_back(Rectangle(Point(free.getX0(), best.getY1()), Point(best.getX1(), free.getY1())));
        output.push_back(Rectangle(Point(best.getX1(), free.getY0()), free.getP1()));
    }

    return output;
}